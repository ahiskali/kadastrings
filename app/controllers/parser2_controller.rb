class ParserController < ApplicationController

  def search
    source = HTTParty.get("http://rosreestr.ru/api/online/fir_object/" + params[:query])
    if source.parsed_response
      @response = JSON.parse(source.body)
      source = HTTParty.get("http://rosreestr.ru/api/online/fir_object/" + @response["objectId"])
      @response = JSON.parse(source.body)
      @response = russify_hash(@response)
    else
      flash.now[:danger] = "Такого кадастрового номера не найдено"
    end
    render 'index'
  end

  private
  def russify_hash(hash)
    russified = {}

    # hash.each do |key, value|
    #   if value.is_a?(Hash)
    #     russified = russified.merge(russify_hash(value))
    #   elsif value.is_a?(Array)
    #     russified = russified.merge(russify_hash(value[0]))
    #   else
    #     key = russify_key(key)
    #     russified[key] = value
    #   end
    # end
    hash.each do |key, value|
      if value.is_a?(Hash)
        value = russify_hash(value)
      elsif value.is_a?(Array)
        value = value.map { |v| russify_hash(v)}
      end
      key = russify_key(key)
      russified[key] = value
    end
    return russified
  end

  def russify_key(key)
    if new_key = Translation.find_by(word: key)
      key = new_key.translation
    end
    return key
  end
end
