class TranslationsController < ApplicationController

  def index
    setup_translations
  end

  def create
    @translation = Translation.new(params[:translation].permit(:word, :translation))
    if @translation.save
      redirect_to translations_path, :notice => "Замена успешно добавлена."
    else
      setup_translations
      render :action => :index
    end
  end

  def show
  end

  def edit
    @translation = Translation.find(params[:id])
  end

  def update
    @translation = Translation.find(params[:id])
    if @translation.update_attributes(params[:translation].permit(:word, :translation))
      redirect_to translations_path
    else
      render 'edit'
    end
  end

  def destroy
    Translation.find(params[:id]).destroy
    flash[:success] = "User deleted"
    redirect_to translations_path
  end

  private

  def setup_translations
    @translations = Translation.all
    @translation ||= Translation.new
  end


end
