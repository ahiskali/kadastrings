class CreateTranslations < ActiveRecord::Migration
  def change
    create_table :translations do |t|
      t.string :word
      t.string :translation

      t.timestamps null: false
    end
  end
end
