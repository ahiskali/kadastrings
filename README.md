# Kadastrings

Prints pretty cadaster using [Rosreestr API] (https://apirosreestr.ru/)

It's live at [https://kadastrings.herokuapp.com/](https://kadastrings.herokuapp.com/)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
